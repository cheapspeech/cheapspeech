import express from "express";
import { Request, Response } from "express"
import RemarkResource from "./remark_resource";
import UserResource from "./user_resource";
import VotesResource from "./votes_resource"
import UserValidation from "./utility/user_validation";
import busboyConnect from "connect-busboy"
import Preprocess from "./utility/preprocess";
import RuntimeConfigs from "./persistence/runtime_configs";
import TrendingResource from "./trending_resource";
import RepliesResource from "./replies_resource";
import Logger from "./utility/logger"
import FollowResource from "./follow_resource";

class AppGlobal {
  static app = express();

  static async initialize() {
    await Preprocess.initialize()

    AppGlobal.app.use(express.json())
    AppGlobal.app.use(busboyConnect())
    AppGlobal.createRoutes()
    AppGlobal.app.use((error: Error, req: any, res: any, next: any) => {
      Logger.appendToLog(error.name)
      Logger.appendToLog(error.message)
      Logger.appendToLog(error.stack ?? "")
      Logger.appendToLog("\n")
      res.status(500).type("application/json").send(JSON.stringify({
        msg: "An unidentified error occured."
      }))
    })
    AppGlobal.app.listen(RuntimeConfigs.port);
  }

  static createRoutes() {
    AppGlobal.app.get("/api/remark", RemarkResource.get)
    AppGlobal.app.post("/api/remark", [UserValidation.verifyToken, RemarkResource.post])
    AppGlobal.app.post("/api/remark/file", [UserValidation.verifyToken, RemarkResource.postFile])
    AppGlobal.app.get("/api/remark/file", RemarkResource.getFile)
    AppGlobal.app.get("/api/remark/user", RemarkResource.getUserOwnedRemarks)
    AppGlobal.app.patch("/api/remark", [UserValidation.verifyToken, RemarkResource.patch])
    AppGlobal.app.delete("/api/remark", [UserValidation.verifyToken, RemarkResource.delete])

    AppGlobal.app.post("/api/votes", [UserValidation.verifyToken, VotesResource.post])
    AppGlobal.app.get("/api/votes/user", [UserValidation.verifyToken, VotesResource.getUser])

    AppGlobal.app.post("/api/follow/follow", [UserValidation.verifyToken, FollowResource.follow])
    AppGlobal.app.post("/api/follow/unfollow", [UserValidation.verifyToken, FollowResource.unfollow])
    AppGlobal.app.get("/api/follow/following", [UserValidation.verifyToken, FollowResource.getFollowing])
    AppGlobal.app.get("/api/follow/isFollowing", [UserValidation.verifyToken, FollowResource.getIsFollowing])
    AppGlobal.app.get("/api/follow/count", [FollowResource.getFollowerCount])

    AppGlobal.app.get("/api/trending", TrendingResource.get)

    AppGlobal.app.get("/api/replies", RepliesResource.get)

    AppGlobal.app.get("/api/user", UserResource.get)
    AppGlobal.app.post("/api/user", UserResource.post)
    AppGlobal.app.patch("/api/user", [UserValidation.verifyToken, UserResource.patch])
    AppGlobal.app.delete("/api/user", [UserValidation.verifyToken, UserResource.delete])
    AppGlobal.app.post("/api/user/login", UserResource.login)
    AppGlobal.app.post("/api/user/logout", [UserValidation.verifyToken, UserResource.logout])
    AppGlobal.app.post("/api/user/logoutAll", [UserValidation.verifyToken, UserResource.logoutAll])

    AppGlobal.app.post("/heartbeat", (req: Request, res: Response, next: Function) => {
      res.sendStatus(200)
    })
  }
}

AppGlobal.initialize()