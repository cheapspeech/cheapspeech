import GetManifest from "../background_work/get_manifest";
import RuntimeConfigs from "../persistence/runtime_configs";


export default class Preprocess {
    static async initialize() {
        await RuntimeConfigs.readConfigs()
        GetManifest.startManifestLoop()
    }
}