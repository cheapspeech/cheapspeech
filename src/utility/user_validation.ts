import bcryptjs from "bcryptjs"
import { Response, Request } from "express"
import DbConnectionFactory from "./db_connection_factory";
import cassandra from "cassandra-driver";


export default class UserValidation {
    static async verifyPassword(username: string, password: string, con: cassandra.Client): Promise<boolean> {
        try {
            var result = await con.execute("select username, password, salt from users where username = ?", [username])
            if (result.rows.length < 1) {
                return false;
            }
            var salt = result.rows[0].salt
            var retrievedHash = result.rows[0].password
            var hashedPassword = await bcryptjs.hash(password, salt)
            return retrievedHash == hashedPassword;
        }
        catch (err) {
            return false;
        }

    }

    static async verifyOwnership(username: string, remarkId: string, con: cassandra.Client): Promise<boolean> {
        try {
            var result = await con.execute("select * from remarkownership where username = ? and remarkid = ?", [
                username,
                remarkId
            ])

            return result.rows.length > 0
        }
        catch (err) {
            return false;
        }
    }

    static async verifyToken(req: Request, res: Response, next: Function) {
        try {

            if (!(req.headers.hasOwnProperty("session-token"))) {
                res.status(422).type("application/json").send(JSON.stringify({
                    msg: "Missing session-token request header."
                }))
                return;
            }

            let sessionToken: string = req.headers["session-token"] as string
            if (!sessionToken) {
                res.status(422).type("application/json").send(JSON.stringify({
                    msg: "session-token request header must not be null or empty."
                }))
                return;
            }
            let con = await DbConnectionFactory.leaseConnection()
            var result = await con.execute("select username from sessions where accesstoken = ?", [sessionToken])


            if (result.rows.length < 1) {
                res.status(401).type("application/json").send(JSON.stringify({
                    msg: "session-token header value not valid for user."
                }))
                await DbConnectionFactory.returnConnection(con)
                return;
            }

            await DbConnectionFactory.returnConnection(con)
            res.locals.username = result.rows[0].username
            res.locals.hasValidSessionToken = true
            next()
        }
        catch (err) {
            next(err)
            return;
        }

    }
}