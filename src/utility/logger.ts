import fs from "fs/promises"

export default class Logger {
    static async appendToLog(msg: string) {
        try {
            await fs.appendFile("log.txt", msg)
        }
        catch (e) {
            return;
        }
    }

    static async appendErrorToLog(err: Error) {
        Logger.appendToLog(err.name)
        Logger.appendToLog(err.message)
        Logger.appendToLog(err.stack ?? "")
        Logger.appendToLog("\n")
    }
}