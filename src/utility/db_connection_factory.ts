import RuntimeConfigs from "../persistence/runtime_configs";
import { Client } from "cassandra-driver"

const latentConnectionsPoolSize = 100

export default class DbConnectionFactory {

    static async leaseConnection(): Promise<Client> {
        var client = new Client({
            keyspace: RuntimeConfigs.databaseName,
            localDataCenter: "datacenter1",
            contactPoints: [RuntimeConfigs.databaseHost]
        })
        await client.connect()
        return client;
    }

    static async returnConnection(con: Client) {
        con.shutdown()
    }
}