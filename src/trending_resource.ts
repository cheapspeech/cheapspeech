import { Request, Response } from "express"
import DbConnectionFactory from "./utility/db_connection_factory"
import { Client } from "cassandra-driver"
import Logger from "./utility/logger"

export default class TrendingResource {

    static get(req: Request, res: Response, next: Function) {
        TrendingResource.wrapCall(async () => {
            var obj = new GetTrending(req, res)
            await obj.getTrending()
        }, res, next)
    }

    static async wrapCall(callback: Function, res: Response, next: Function) {
        try {
            await callback()
        }
        catch (err) {
            TrendingResource.errorResponse(res, err, next)
        }
    }

    static errorResponse(res: Response, err: any, next: Function) {
        if (err instanceof TrendingResourceError) {
            res.status(err.code).type("application/json").send(err.msg)
        }
        else {
            Logger.appendErrorToLog(err)
            res.status(500).type("application/json").send(JSON.stringify({
                msg: "An unknown error occured."
            }))
        }
    }
}

class GetTrending {
    req: Request;
    res: Response;

    constructor(req: Request, res: Response) {
        this.req = req
        this.res = res
    }

    async getTrending() {
        this.validateRequest()
        var start = parseInt(this.req.query.start as string)
        var length = parseInt(this.req.query.length as string)
        if (isNaN(start) || isNaN(length)) {
            throw new TrendingResourceError(JSON.stringify({
                msg: "Could not parse number out of start or length keys"
            }), 422)
        }

        var positions = Array.from(Array(length + 1).keys(), (v: number, x: number) => {
            return x + start
        })
        var clause = positions.join(",")
        var con = await DbConnectionFactory.leaseConnection()
        var results = await con.execute(`select remarkid, position from trending where position in (${clause})`)

        var remarkIds: any = {}

        for (var row of results.rows) {
            var position = row.position as number
            remarkIds[position] = row.remarkid
        }

        var sortedPositions: Array<string> = Object.keys(remarkIds).sort()
        var sortedRemarkIds: Array<string> = []

        for (var positionString of sortedPositions) {
            sortedRemarkIds.push(remarkIds[positionString])
        }

        this.res.status(200).type("application/json").send(JSON.stringify(sortedRemarkIds))
        await DbConnectionFactory.returnConnection(con);
    }

    validateRequest() {
        if (!this.req.query) {
            throw new TrendingResourceError(JSON.stringify({
                msg: "Missing query params"
            }), 422)
        }
        else if (!this.req.query.hasOwnProperty(("start"))) {
            throw new TrendingResourceError(JSON.stringify({
                msg: "Missing `start` property from query params"
            }), 422)
        }
        else if (!this.req.query.hasOwnProperty(("length"))) {
            throw new TrendingResourceError(JSON.stringify({
                msg: "Missing `length` property from query params"
            }), 422)
        }
    }

}

class TrendingResourceError extends Error {
    code: number = 500;
    msg: string = "";

    constructor(msg: string, code: number) {
        super(msg);
        this.msg = msg;
        this.code = code;
    }
}