import fs from "fs/promises"

export default class RuntimeConfigs {
    static accessToken: string = "";
    static port: string = ""
    static address: string = ""
    static coordinatorAddress: string = ""
    static coordinatorPort: string = ""
    static databasePassword: string = ""
    static databaseUsername: string = ""
    static databaseName: string = ""
    static databaseHost: string = ""
    static publicServerUrl: string = ""

    static async readConfigs() {
        try {
            var configFile = await fs.readFile("./conf/app_config.json")
        }
        catch (err) {
            throw new Error("Could not read config file ./conf/app_config.json")

        }

        var configFileJson = JSON.parse(configFile.toString())
        if (!configFileJson.hasOwnProperty("accessToken")) {
            throw new Error("Config file missing accessToken attribute")
        }
        else if (!configFileJson.hasOwnProperty("port")) {
            throw new Error("Config file missing port attribute")
        }
        else if (!configFileJson.hasOwnProperty("coordinatorAddress")) {
            throw new Error("Config file missing coordinatorAddress attribute")
        }
        else if (!configFileJson.hasOwnProperty("coordinatorPort")) {
            throw new Error("Config file missing coordinatorPort attribute")
        }
        else if (!configFileJson.hasOwnProperty("address")) {
            throw new Error("Config file missing address attribute")
        }
        else if (!configFileJson.hasOwnProperty("databasePassword")) {
            throw new Error("Config file missing databasePassword attribute")
        }
        else if (!configFileJson.hasOwnProperty("databaseUsername")) {
            throw new Error("Config file missing databaseUsername attribute")
        }
        else if (!configFileJson.hasOwnProperty("databaseName")) {
            throw new Error("Config file missing databaseName attribute")
        }
        else if (!configFileJson.hasOwnProperty("databaseHost")) {
            throw new Error("Config file missing databaseHost attribute")
        }
        else if (!configFileJson.hasOwnProperty("publicServerUrl")) {
            throw new Error("Config file missing publicServerUrl attribute")
        }

        RuntimeConfigs.accessToken = configFileJson.accessToken
        RuntimeConfigs.port = configFileJson.port
        RuntimeConfigs.address = configFileJson.address
        RuntimeConfigs.coordinatorAddress = configFileJson.coordinatorAddress
        RuntimeConfigs.coordinatorPort = configFileJson.coordinatorPort
        RuntimeConfigs.databasePassword = configFileJson.databasePassword
        RuntimeConfigs.databaseUsername = configFileJson.databaseUsername
        RuntimeConfigs.databaseName = configFileJson.databaseName
        RuntimeConfigs.databaseHost = configFileJson.databaseHost
        RuntimeConfigs.publicServerUrl = configFileJson.publicServerUrl
    }
}