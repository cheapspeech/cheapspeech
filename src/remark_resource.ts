import { Request, Response } from "express";
import { Client } from "cassandra-driver"
import DbConnectionFactory from "./utility/db_connection_factory";
import axios from "axios"
import FormData from "form-data"
import GetManifest from "./background_work/get_manifest";
import { Manifest, ResourceId } from "cheapspeech-shared"
import RuntimeConfigs from "./persistence/runtime_configs";
import UserValidation from "./utility/user_validation";
import Logger from "./utility/logger";

export default class RemarkResource {

    static get(req: Request, res: Response) {
        RemarkResource.wrapCall(async () => {
            var obj = new GetRemark(req, res);
            await obj.execute();
        }, res)
    }

    static patch(req: Request, res: Response) {
        RemarkResource.wrapCall(async () => {
            var obj = new PatchRemark(req, res);
            await obj.execute();
        }, res)
    }

    static post(req: Request, res: Response) {
        RemarkResource.wrapCall(async () => {
            var obj = new PostRemark(req, res);
            await obj.execute();
        }, res)
    }

    static postFile(req: Request, res: Response) {
        RemarkResource.wrapCall(async () => {
            var obj = new PostRemarkFile(req, res);
            await obj.execute();
        }, res)
    }

    static getFile(req: Request, res: Response) {
        RemarkResource.wrapCall(async () => {
            var obj = new GetRemarkFile(req, res);
            await obj.execute();
        }, res)
    }

    static getUserOwnedRemarks(req: Request, res: Response) {
        RemarkResource.wrapCall(async () => {
            var obj = new GetUserOwnedRemarks(req, res);
            await obj.execute();
        }, res)
    }

    static delete(req: Request, res: Response) {
        RemarkResource.wrapCall(async () => {
            var obj = new DeleteRemark(req, res);
            await obj.execute();
        }, res)
    }

    static async wrapCall(callback: Function, res: Response) {
        try {
            await callback()
        }
        catch (err) {
            RemarkResource.errorResponse(res, err)
        }
    }

    static errorResponse(res: Response, err: Error) {
        if (err instanceof RemarkResourceError) {
            res.status(err.code).type("application/json").send(err.msg)
        }
        else {
            Logger.appendErrorToLog(err)
            res.status(500).type("application/json").send(JSON.stringify({
                msg: "An unidentified error occured."
            }))
        }
    }
}

class GetRemark {
    req: Request;
    res: Response;

    constructor(req: Request, res: Response) {
        this.req = req
        this.res = res
    }
    async execute() {
        this.validateCall()
        try {
            let con = await DbConnectionFactory.leaseConnection()
            var result = await con.execute("select * from remarks where remarkid = ?", [this.req.query.remarkId])
            await DbConnectionFactory.returnConnection(con)
        }
        catch (err) {
            throw new RemarkResourceError(JSON.stringify({
                msg: "A sql server error occured."
            }), 500)
        }

        if (result.rows.length < 1) {
            throw new RemarkResourceError(JSON.stringify({
                msg: "Could not find remark with requested id."
            }), 404)
        }

        var upvotes = parseInt(result.rows[0].upvotes)
        var downvotes = parseInt(result.rows[0].downvotes)

        this.res.status(200).type("application/json").send(JSON.stringify({
            title: result.rows[0].title,
            text: result.rows[0].text,
            type: result.rows[0].type,
            tags: JSON.parse(result.rows[0].tags),
            owner: result.rows[0].ownerusername,
            images: JSON.parse(result.rows[0].images).map(this.createFileUrl),
            video: JSON.parse(result.rows[0].video).map(this.createFileUrl),
            parentId: result.rows[0].parentid,
            upvotes: upvotes ? upvotes : 0,
            downvotes: downvotes ? downvotes : 0,
            remarkId: this.req.query.remarkId
        }))
    }

    validateCall() {
        if (!this.req.query) {
            throw new RemarkResourceError(JSON.stringify({
                msg: "Missing url search params."
            }), 422)
        }
        if (!this.req.query.hasOwnProperty("remarkId")) {
            throw new RemarkResourceError(JSON.stringify({
                msg: "Missing remarkId search param."
            }), 422)
        }
    }

    createFileUrl(image: string): string {
        return RuntimeConfigs.publicServerUrl + "/api/remark/file?fileName=" + image
    }
}

class GetUserOwnedRemarks {
    req: Request;
    res: Response;

    constructor(req: Request, res: Response) {
        this.req = req
        this.res = res
    }
    async execute() {
        this.validateCall()
        var con = await DbConnectionFactory.leaseConnection()
        var username = await this.getUsername(con)
        var [pageState, remarks] = await this.getNextRemarks(con, username);
        await DbConnectionFactory.returnConnection(con)
        this.res.status(200).type("application/json").send(JSON.stringify({
            remarks: remarks,
            pageState: pageState
        }))
    }

    validateCall() {
        if (!this.req.query) {
            throw new RemarkResourceError(JSON.stringify({
                msg: "Missing query params."
            }), 422)
        }
        else if (!this.req.query.hasOwnProperty("length")) {
            throw new RemarkResourceError(JSON.stringify({
                msg: "Missing length param."
            }), 422)
        }
    }

    async getUsername(con: Client): Promise<string> {
        if (this.req.query.hasOwnProperty("username")) {
            return this.req.query.username as string;
        }
        else {
            try {
                return await this.getUsernameFromToken(this.req, con);
            }
            catch (e) {
                throw new RemarkResourceError(JSON.stringify({
                    msg: "Could not find user from token."
                }), 500)
            }
        }
    }

    async getNextRemarks(con: Client, username: string): Promise<Array<any>> {
        var length = parseInt(this.req.query.length as string)
        if (this.req.query.hasOwnProperty("pageState")) {
            var remarks = await con.execute(
                "select remarkid from remarkownership where username = ?",
                [username],
                {
                    pageState: this.req.query.pageState as string,
                    fetchSize: length
                }
            )
        }
        else {
            var remarks = await con.execute(
                "select remarkid from remarkownership where username = ?",
                [username],
                {
                    fetchSize: length
                }
            )
        }

        var remarkIds = remarks.rows.map((elem) => {
            return elem.get("remarkid")
        })
        var pageState = remarks.pageState;

        return [pageState, remarkIds];
    }

    async getUsernameFromToken(req: Request, con: Client): Promise<string> {
        try {
            let sessionToken: string = req.headers["session-token"] as string
            if (!sessionToken) {
                throw new Error("Session token header not included.")
            }
            var result = await con.execute("select username from sessions where accesstoken = ?", [sessionToken])
            if (result.rows.length < 1) {
                throw new Error("Could not find user from token.")
            }
            return result.rows[0].username
        }
        catch (err) {
            return "";
        }

    }
}

class PatchRemark {
    req: Request;
    res: Response;

    constructor(req: Request, res: Response) {
        this.req = req
        this.res = res
    }
    execute() { }
}

class PostRemark {
    req: Request;
    res: Response;
    static videoType: string = "video";
    static imageType: string = "images";
    static textType: string = "text"

    constructor(req: Request, res: Response) {
        this.req = req
        this.res = res
    }

    async execute() {
        this.validateInput()
        var remarkId: String = await this.createRemarkRecord()
        this.res.status(200).type("application/json").send(JSON.stringify({
            remarkId: remarkId,
            type: this.req.body.type
        }))
    }

    validateInput() {
        var body = this.req.body
        var requiredProperties = [
            "title",
            "text",
            "tags",
            "type"
        ]
        requiredProperties.forEach(e => {
            if (!body.hasOwnProperty(e)) {
                throw new RemarkResourceError(JSON.stringify({
                    msg: `Missing ${e} property`
                }), 422)
            }
        })

        if (!(body.tags instanceof Array)) {
            throw new RemarkResourceError(JSON.stringify({
                msg: "tags must be an array of strings."
            }), 422)
        }
        else if (!([PostRemark.videoType, PostRemark.imageType, PostRemark.textType].includes(body.type))) {
            throw new RemarkResourceError(JSON.stringify({
                msg: "type must be one of 'text', 'images' or 'video'"
            }), 422)
        }
        else if (!(typeof body.title === "string")) {
            throw new RemarkResourceError(JSON.stringify({
                msg: "title must be a string"
            }), 422)
        }
        else if (!(typeof body.text === "string")) {
            throw new RemarkResourceError(JSON.stringify({
                msg: "text must be a string"
            }), 422)
        }
    }

    async createRemarkRecord(): Promise<String> {
        var body = this.req.body
        var con = await DbConnectionFactory.leaseConnection()
        var id = await this.getRemarkId(con)
        var tagsString = body.tags ? JSON.stringify(body.tags) : "[]"
        var parentId = body.hasOwnProperty("parentId") ? body.parentId : ""
        try {
            // Create record.
            await con.execute(`insert into remarks (remarkid, ownerusername, title, text, tags, type, video, images, parentid) values
            (?, ?, ?, ?, ?, ?, ?, ?, ?)`, [
                id,
                this.res.locals.username,
                body.title,
                body.text,
                tagsString,
                body.type,
                "[]",
                "[]",
                parentId
            ])

            await this.addRemarkOwnershipRecord(id, this.res.locals.username, con);

            // Create reply record.
            if (parentId) {
                await this.createReplyRecord(parentId, id, con)
            }
        }
        catch (err) {
            throw new RemarkResourceError(JSON.stringify({
                msg: "Cql error"
            }), 500)
        }

        await DbConnectionFactory.returnConnection(con)
        return id;
    }

    async addRemarkOwnershipRecord(id: string, username: string, con: Client) {
        await con.execute("insert into remarkownership (remarkid, username) values (?, ?)", [
            id,
            username
        ]);
    }

    async createReplyRecord(remarkId: string, replyId: string, con: Client): Promise<boolean> {
        try {
            await con.execute("insert into replies (remarkid, replyid) values (?, ?)", [remarkId, replyId])
        }
        catch (e) {
            return false;
        }
        return true;
    }

    async getRemarkId(con: Client): Promise<string> {
        var resourceId = new ResourceId()
        do {
            var id = resourceId.getNewId()
            try {
                var result = await con.execute("select * from remarks where remarkid = ?", [id])
                if (result.rows.length < 1) {
                    break;
                }
            }
            catch (err) {
                throw new RemarkResourceError(JSON.stringify({ msg: "Cql error." }), 500)
            }
        }
        while (true)

        return id;
    }
}

class DeleteRemark {
    req: Request;
    res: Response;

    constructor(req: Request, res: Response) {
        this.req = req
        this.res = res
    }

    async execute() {
        this.validateCall();
        var con = await DbConnectionFactory.leaseConnection()
        await this.verifyOwnership(con)
        await this.deleteFiles(con)
        await this.deleteRemark(con)
        await DbConnectionFactory.returnConnection(con)
        this.res.sendStatus(200)
    }

    validateCall() {
        if (!this.req.query) {
            throw new RemarkResourceError(JSON.stringify({
                msg: "Missing query params."
            }), 422)
        }
        else if (!this.req.query.hasOwnProperty("remarkId")) {
            throw new RemarkResourceError(JSON.stringify({
                msg: "Missing remarkId param."
            }), 422)
        }
    }

    async verifyOwnership(con: Client) {
        var userOwnsRemark = await UserValidation.verifyOwnership(
            this.res.locals.username,
            this.req.query.remarkId as string,
            con,
        )

        if (!userOwnsRemark) {
            throw new RemarkResourceError(JSON.stringify({
                msg: "You don't own that remark."
            }), 401)
        }
    }

    async deleteRemark(con: Client) {
        var remarkId = this.req.query.remarkId as string
        var username = this.res.locals.username

        await con.execute("delete from votes where remarkId = ?", [remarkId])
        await con.execute("delete from remarkownership where username = ? and remarkId = ?", [username, remarkId])
        await con.execute(`update remarks set ownerusername = null, title = null, text = null, tags = '[]', images = '[]', 
        video = '[]', upvotes = null, downvotes = null where remarkId = ?`, [remarkId])
    }

    async deleteFiles(con: Client) {
        var remarkId = this.req.query.remarkId as string
        var result = await con.execute("select images, video from remarks where remarkid = ?", [remarkId])
        if (result.rows.length < 1) {
            return;
        }

        var images = JSON.parse(result.rows[0].images)
        var videos = JSON.parse(result.rows[0].video)

        for (var image of images) {
            await DeleteRemarkFile.deleteFile(image)
        }

        for (var video of videos) {
            await DeleteRemarkFile.deleteFile(video)
        }
    }
}

class PostRemarkFile {
    req: Request;
    res: Response;
    maxFileSize: number = 80000000;

    constructor(req: Request, res: Response) {
        this.req = req
        this.res = res
    }

    async execute() {
        this.validateInput()
        this.req.busboy.on("file", async (fieldname, file, filename, encoding, mimetype) => {
            RemarkResource.wrapCall(async () => {
                var resourceId = new ResourceId()
                var splitFileName = filename.split(".")
                var extension = "." + splitFileName[splitFileName.length - 1]
                var fileName = await this.getFileName(extension, resourceId)
                await this.setFileNameInDatabase(fileName as string)
                await this.sendFile(file, fileName as string, resourceId)
            }, this.res)
        })
        this.req.pipe(this.req.busboy)
    }

    validateInput() {
        var bodyLength = parseInt(this.req.headers["content-length"] ?? "0")
        if (!this.req.busboy) {
            throw new RemarkResourceError(JSON.stringify({
                msg: "Missing file data, did you forget to attach the file?"
            }), 422)
        }
        else if (!this.req.query) {
            throw new RemarkResourceError(JSON.stringify({
                msg: "Missing query params."
            }), 422)
        }
        else if (!this.req.query.hasOwnProperty("remarkId")) {
            throw new RemarkResourceError(JSON.stringify({
                msg: "Missing remark id from query params."
            }), 422)
        }
        else if (!GetManifest.manifest) {
            throw new RemarkResourceError(JSON.stringify({
                msg: "Manifest was null."
            }), 500)
        }
        else if (bodyLength > this.maxFileSize) {
            throw new RemarkResourceError(JSON.stringify({
                msg: "Max file upload size of 30Mb exceeded."
            }), 413)
        }

    }

    async getFileName(extension: string, resourceId: ResourceId): Promise<String> {

        do {
            var hash = resourceId.getNewId()
            var fileName = hash + extension
            var schemas = GetManifest?.manifest?.activeHashSchemas
            var locations = resourceId.getPotentialLocations(hash, schemas)

            try {
                // Check each currently active read schema to see if resource id exists or not.
                for (var location of locations) {
                    var address = GetManifest?.manifest?.fileShardAddresses.find((elem) => {
                        return elem.shardNumber == location
                    })
                    if (!address) {
                        continue;
                    }

                    var url = new URL("http://" + address.address + ":" + address.port + "/file/exists")
                    url.search = new URLSearchParams({ fileName: fileName }).toString()
                    let request = await axios.get(url.toString())
                    if (!request.data.fileExists) {
                        return fileName
                    }
                }
            }
            catch (err) {
                continue;
            }
        }
        while (true)
    }

    async setFileNameInDatabase(fileName: string) {
        // Get referenced remark
        try {
            var con: Client = await DbConnectionFactory.leaseConnection()
            var result = await con.execute("select images, video, type from remarks where remarkid = ?", [this.req.query.remarkId])

        }
        catch (err) {
            throw new RemarkResourceError(JSON.stringify({
                msg: "A sql error occured"
            }), 500)
        }

        // If remark is not found, throw
        if (result.rows.length < 1) {
            throw new RemarkResourceError(JSON.stringify({
                msg: "Could not find requested remark."
            }), 404)
        }

        // Parse current images / video fields
        let images = JSON.parse(result.rows[0].images)
        images = images ? images : []
        let videos = JSON.parse(result.rows[0].video)
        videos = videos ? videos : []
        let type = result.rows[0].type

        // Videos get one upload
        if (type == PostRemark.videoType) {
            if (videos.length >= 1) {
                throw new RemarkResourceError(JSON.stringify({
                    msg: "Only one video may be uploaded per post."
                }), 402)
            }
            else {
                videos.push(fileName)
            }
        }
        // Images get four uploads
        else if (type == PostRemark.imageType) {
            if (images.length >= 4) {
                throw new RemarkResourceError(JSON.stringify({
                    msg: "Only four images may be uploaded per post."
                }), 402)
            }
            else {
                images.push(fileName)
            }
        }
        // Text gets no uploads
        else if (type == PostRemark.textType) {
            throw new RemarkResourceError(JSON.stringify({
                msg: "Text only posts do not support file uploads"
            }), 402)
        }

        try {
            await con.execute("update remarks set images = ?, video = ? where remarkid = ?", [
                JSON.stringify(images),
                JSON.stringify(videos),
                this.req.query.remarkId
            ])
        }
        catch (err) {
            console.log(err)
            throw new RemarkResourceError(JSON.stringify({
                msg: "A sql error occured"
            }), 500)
        }
    }

    async sendFile(file: NodeJS.ReadableStream, name: string, resourceId: ResourceId) {
        var targetSchema = GetManifest.manifest?.targetHashSchema as number
        var fileHash = this.removeFileExtension(name)
        var target = resourceId.getShardNumber(fileHash, targetSchema)
        var address = GetManifest.manifest?.fileShardAddresses.find((elem) => {
            return elem.shardNumber == target
        })

        if (!address) {
            throw new RemarkResourceError(JSON.stringify({
                msg: "Could not find shard details for requested shard."
            }), 500)
        }

        var url = new URL("http://" + address.address + ":" + address.port + "/file")
        url.search = new URLSearchParams({
            fileName: name
        }).toString()

        try {
            var formData = new FormData()
            formData.append("data", file)
            await axios.post(url.toString(), formData, {
                headers: formData.getHeaders(),
                maxBodyLength: this.maxFileSize,
                maxContentLength: this.maxFileSize,
            })

            this.res.status(200).type("application/json").send(JSON.stringify({
                msg: "File sucessfully uploaded."
            }))
        }
        catch (err) {
            throw new RemarkResourceError(JSON.stringify({
                msg: "Failed to save uploaded file."
            }), 500)
        }
    }

    removeFileExtension(fileName: string): string {
        var split = fileName.split(".")
        if (split.length > 1) {
            return split[0]
        }
        else {
            return ""
        }
    }
}

class GetRemarkFile {
    req: Request;
    res: Response;

    constructor(req: Request, res: Response) {
        this.req = req
        this.res = res
    }

    async execute() {
        this.validateInput()
        await this.getFile(GetManifest.manifest as Manifest)
    }

    validateInput() {
        if (!this.req.query) {
            throw new RemarkResourceError(JSON.stringify({
                msg: "Missing query params."
            }), 422)
        }
        else if (!this.req.query.hasOwnProperty("fileName")) {
            throw new RemarkResourceError(JSON.stringify({
                msg: "Missing fileName from query params."
            }), 422)
        }
        else if (!GetManifest.manifest) {
            throw new RemarkResourceError(JSON.stringify({
                msg: "Manifest was null."
            }), 500)
        }
    }

    async getFile(manifest: Manifest) {
        var fileName: string = this.req.query.fileName as string;
        var hash = this.removeFileExtension(fileName)
        var schemas = manifest.activeHashSchemas
        var resourceId = new ResourceId()
        var locations = resourceId.getPotentialLocations(hash, schemas)

        for (var location of locations) {
            var address = manifest.fileShardAddresses.find((elem) => {
                return elem.shardNumber == location
            })

            var success = await this.tryLocation(address?.address, address?.port)

            if (success) {
                return;
            }
        }

        this.res.status(404).type("application/json").send(JSON.stringify({
            msg: "Could not find requested file."
        }))
    }

    async tryLocation(address: string | undefined, port: string | undefined): Promise<boolean> {
        if (!address || !port) {
            return false;
        }

        var url = new URL("http://" + address + ":" + port + "/file")
        url.search = new URLSearchParams({
            fileName: this.req.query.fileName as string
        }).toString()


        var response = await axios.get(
            url.toString(),
            {
                responseType: "arraybuffer",
                validateStatus: (status: number) => {
                    return true;
                }
            }
        )

        if (response.status == 200) {
            for (const prop in response.headers) {
                this.res.setHeader(prop, response.headers[prop])
            }
            this.res.status(200).send(response.data)
            return true;
        }
        else {
            return false;
        }
    }

    removeFileExtension(fileName: string): string {
        var split = fileName.split(".")
        if (split.length > 1) {
            return split[0]
        }
        else {
            return ""
        }
    }
}

class DeleteRemarkFile {
    req: Request;
    res: Response;

    constructor(req: Request, res: Response) {
        this.req = req
        this.res = res
    }

    async execute() { }

    static async deleteFile(fileName: string) {
        var manifest = GetManifest.manifest
        var hash = fileName.split(".")[0]
        var schemas = manifest?.activeHashSchemas
        var resourceId = new ResourceId()
        var locations = resourceId.getPotentialLocations(hash, schemas)

        for (var location of locations) {
            var address = manifest?.fileShardAddresses.find((elem) => {
                return elem.shardNumber == location
            })

            var success = await this.tryLocation(address?.address, address?.port, fileName)

            if (success) {
                return;
            }
        }
    }

    static async tryLocation(address: string | undefined, port: string | undefined, fileName: string): Promise<boolean> {
        var url = new URL("http://" + address + ":" + port + "/file")
        url.search = new URLSearchParams({
            fileName: fileName
        }).toString()


        var response = await axios.delete(
            url.toString(),
            {
                validateStatus: (status: number) => {
                    return true;
                }
            }
        )

        return response.status == 200
    }
}

class RemarkResourceError extends Error {
    code: number = 200;
    msg: string = "";

    constructor(msg: string, code: number) {
        super(msg);
        this.msg = msg;
        this.code = code;
    }
}