import { Request, Response } from "express"
import DbConnectionFactory from "./utility/db_connection_factory"
import { Client } from "cassandra-driver"
import Logger from "./utility/logger"

const validVoteTypes = {
    upvote: "upvote",
    downvote: "downvote",
    none: ""
}

export default class VotesResource {
    static post(req: Request, res: Response, next: Function) {
        VotesResource.wrapCall(async () => {
            var manageReactions = new ManageVotes(req, res)
            await manageReactions.createVote()
        }, res, next)
    }

    static getUser(req: Request, res: Response, next: Function) {
        VotesResource.wrapCall(async () => {
            var manageReactions = new ManageVotes(req, res)
            await manageReactions.getDidVoteOnRemark()
        }, res, next)
    }

    static async wrapCall(callback: Function, res: Response, next: Function) {
        try {
            await callback()
        }
        catch (err) {
            VotesResource.errorResponse(res, err, next)
        }
    }

    static errorResponse(res: Response, err: any, next: Function) {
        if (err instanceof VotesResourceError) {
            res.status(err.code).type("application/json").send(err.msg)
        }
        else {
            Logger.appendErrorToLog(err)
            next(err)
        }
    }
}

class ManageVotes {
    req: Request;
    res: Response;

    constructor(req: Request, res: Response) {
        this.req = req
        this.res = res
    }

    async createVote() {
        this.validateCreateVote()
        var con = await DbConnectionFactory.leaseConnection()
        var currentVote = await this.getHasUserVoted(con)
        var [upvotes, downvotes] = await this.setNewVote(con, currentVote)
        await DbConnectionFactory.returnConnection(con)
        this.res.status(200).type("application/json").send(JSON.stringify({
            "upvotes": upvotes,
            "downvotes": downvotes
        }))
    }

    validateCreateVote() {
        if (!this.req.body.hasOwnProperty("remarkId")) {
            throw new VotesResourceError(JSON.stringify({
                msg: "Missing remarkId key."
            }), 422)
        }
        else if (!this.req.body.hasOwnProperty("type")) {
            throw new VotesResourceError(JSON.stringify({
                msg: "Missing type key."
            }), 422)
        }
        else if (!Object.values(validVoteTypes).includes(this.req.body.type)) {
            throw new VotesResourceError(JSON.stringify({
                msg: "Type key must be one of these:",
                types: Object.values(validVoteTypes)
            }), 422)
        }
    }

    async getHasUserVoted(con: Client): Promise<string> {
        var result = await con.execute("select username, type from votes where remarkid = ? and username = ?", [
            this.req.body.remarkId,
            this.res.locals.username
        ])

        // Vote row not created
        if (result.rows.length < 1) {
            return "";
        }
        // Row type null or undefined
        else if (!result.rows[0].type) {
            return "";
        }
        // Return vote type
        else {
            return result.rows[0].type
        }
    }

    async setNewVote(con: Client, currentReaction: string): Promise<Array<number>> {
        var reactionCounts = await con.execute("select upvotes, downvotes, ownerusername from remarks where remarkId = ?", [this.req.body.remarkId])
        // If the requested remark is irretrievable, fail silently.
        if (reactionCounts.rows.length < 1) {
            return [0, 0];
        }
        // Deleted remarks do not have owner association, do not allow voting
        // on deleted remarks.
        else if (!reactionCounts.rows[0].ownerusername) {
            return [0, 0];
        }

        var upVotes = reactionCounts.rows[0].upvotes
        upVotes = upVotes ? parseInt(upVotes) : 0
        var downVotes = reactionCounts.rows[0].downvotes
        downVotes = downVotes ? parseInt(downVotes) : 0
        var newReaction = this.req.body.type

        if (currentReaction == newReaction) {
            return [upVotes, downVotes];
        }
        else if (currentReaction === validVoteTypes.none) {
            if (newReaction == validVoteTypes.downvote) {
                downVotes++
            }
            else if (newReaction === validVoteTypes.upvote) {
                upVotes++
            }
        }
        else if (currentReaction === validVoteTypes.upvote) {
            if (newReaction === validVoteTypes.downvote) {
                downVotes++
                upVotes--
            }
            else if (newReaction === validVoteTypes.none) {
                upVotes--
            }
        }
        else if (currentReaction === validVoteTypes.downvote) {
            if (newReaction === validVoteTypes.upvote) {
                downVotes--
                upVotes++
            }
            else if (newReaction === validVoteTypes.none) {
                downVotes--
            }
        }
        else {

            return [0, 0];
        }

        await con.execute("update remarks set upvotes = ?, downvotes = ? where remarkid = ?", [
            upVotes,
            downVotes,
            this.req.body.remarkId
        ],
            {
                hints: [
                    "varint",
                    "varint",
                    "text"
                ]
            }
        )

        await con.execute("insert into votes (remarkid, username, type) values (?, ?, ?)", [
            this.req.body.remarkId,
            this.res.locals.username,
            newReaction
        ])

        return [upVotes, downVotes];
    }

    async getDidVoteOnRemark() {
        var con = await DbConnectionFactory.leaseConnection()

        if (!this.req.query) {
            throw new VotesResourceError(JSON.stringify({
                msg: "Missing query params."
            }), 422)
        }
        else if (!this.req.query.hasOwnProperty("remarkId")) {
            throw new VotesResourceError(JSON.stringify({
                msg: "Missing remarkId query param."
            }), 422)
        }

        var result = await con.execute("select username, type from votes where remarkid = ? and username = ?", [
            this.req.query.remarkId,
            this.res.locals.username
        ])

        if (result.rows.length < 1 || result.rows[0].type == validVoteTypes.none) {
            this.res.status(200).type("application/json").send(JSON.stringify({
                hasVoted: false,
                type: ""
            }))
        }
        else {
            this.res.status(200).type("application/json").send(JSON.stringify({
                hasVoted: true,
                type: result.rows[0].type
            }))
        }

        await DbConnectionFactory.returnConnection(con)
    }
}

class VotesResourceError extends Error {
    code: number = 500;
    msg: string = "";

    constructor(msg: string, code: number) {
        super(msg);
        this.msg = msg;
        this.code = code;
    }
}