import { Request, Response } from "express"
import DbConnectionFactory from "./utility/db_connection_factory"
import { Client } from "cassandra-driver"
import Logger from "./utility/logger"

export default class RepliesResource {

    static get(req: Request, res: Response, next: Function) {
        RepliesResource.wrapCall(async () => {
            var obj = new GetReplies(req, res)
            await obj.getReplies()
        }, res, next)
    }

    static async wrapCall(callback: Function, res: Response, next: Function) {
        try {
            await callback()
        }
        catch (err) {
            RepliesResource.errorResponse(res, err, next)
        }
    }

    static errorResponse(res: Response, err: any, next: Function) {
        if (err instanceof RepliesResourceError) {
            res.status(err.code).type("application/json").send(err.msg)
        }
        else {
            Logger.appendErrorToLog(err)
            res.status(500).type("application/json").send(JSON.stringify({
                msg: "An unknown error occured."
            }))
        }
    }
}

class GetReplies {
    req: Request;
    res: Response;

    constructor(req: Request, res: Response) {
        this.req = req
        this.res = res
    }

    async getReplies() {
        this.validateRequest()
        var length = parseInt(this.req.query.length as string)
        var remarkId = this.req.query.remarkId
        var pageState = this.req.query.hasOwnProperty("pageState") ? this.req.query.pageState : null;
        if (isNaN(length)) {
            throw new RepliesResourceError(JSON.stringify({
                msg: "Could not parse number out of start or length keys"
            }), 422)
        }
        var con = await DbConnectionFactory.leaseConnection()
        if (pageState) {
            var results = await con.execute(
                `select replyid from replies where remarkid = ?`,
                [remarkId],
                {
                    hints: ["text"],
                    fetchSize: length,
                    pageState: pageState as string
                })
        }
        else {
            var results = await con.execute(`select replyid from replies where remarkid = ?`,
                [remarkId],
                {
                    hints: ["text", "int"],
                    fetchSize: length
                },
            )
        }

        var nextReplies: Array<string> = [];

        for (var row of results.rows) {
            nextReplies.push(row.get("replyid"))
        }

        this.res.status(200).type("application/json").send(JSON.stringify({
            "replies": nextReplies,
            "pageState": results.pageState
        }))
        await DbConnectionFactory.returnConnection(con);
    }

    validateRequest() {
        if (!this.req.query) {
            throw new RepliesResourceError(JSON.stringify({
                msg: "Missing query params"
            }), 422)
        }
        else if (!this.req.query.hasOwnProperty(("length"))) {
            throw new RepliesResourceError(JSON.stringify({
                msg: "Missing `length` property from query params"
            }), 422)
        }
        else if (!this.req.query.hasOwnProperty(("remarkId"))) {
            throw new RepliesResourceError(JSON.stringify({
                msg: "Missing `remarkId` property from query params"
            }), 422)
        }
        else if (!this.req.query.remarkId) {
            throw new RepliesResourceError(JSON.stringify({
                msg: "`remarkId` property cannot be null or empty."
            }), 422)
        }
    }

}

class RepliesResourceError extends Error {
    code: number = 500;
    msg: string = "";

    constructor(msg: string, code: number) {
        super(msg);
        this.msg = msg;
        this.code = code;
    }
}