import { Request, Response } from "express";
import DbConnectionFactory from "./utility/db_connection_factory"
import bcryptjs from "bcryptjs";
import crypto from "crypto";
import UserValidation from "./utility/user_validation"
import cassandra, { Client } from "cassandra-driver"
import Logger from "./utility/logger";

export default class UserResource {

    static get(req: Request, res: Response, next: Function) {

    }

    static patch(req: Request, res: Response, next: Function) {

    }

    static post(req: Request, res: Response, next: Function) {
        UserResource.wrapCall(async () => {
            var obj = new CreateUser(req, res)
            await obj.execute()
        }, res, next)
    }

    static delete(req: Request, res: Response, next: Function) {

    }

    static login(req: Request, res: Response, next: Function) {
        UserResource.wrapCall(async () => {
            var obj = new ManageSession(req, res)
            await obj.login()
        }, res, next)
    }

    static logout(req: Request, res: Response, next: Function) {
        UserResource.wrapCall(async () => {
            var obj = new ManageSession(req, res)
            await obj.logout()
        }, res, next)
    }

    static logoutAll(req: Request, res: Response, next: Function) {
        UserResource.wrapCall(async () => {
            var obj = new ManageSession(req, res)
            await obj.logoutAll()
        }, res, next)
    }

    static async wrapCall(callback: Function, res: Response, next: Function) {
        try {
            await callback()
        }
        catch (err) {
            UserResource.errorResponse(res, err, next)
        }
    }

    static errorResponse(res: Response, err: any, next: Function) {
        if (err instanceof UserResourceError) {
            res.status(err.code).type("application/json").send(err.msg)
        }
        else {
            Logger.appendErrorToLog(err)
            next(err)
        }
    }
}

class CreateUser {
    req: Request;
    res: Response;
    utils: UserResourceUtilities = new UserResourceUtilities()

    constructor(req: Request, res: Response) {
        this.req = req
        this.res = res
    }

    async execute() {
        var body = this.req.body
        if (body.hasOwnProperty("username") && body.hasOwnProperty("password")) {
            var token = await this.createUser(body["username"], body["password"])
        }
        else {
            throw new UserResourceError(JSON.stringify({
                msg: "Missing username or password property."
            }), 422)
        }

        this.res.status(200)
            .type("applications/json")
            .setHeader("session-token", token)
            .send(JSON.stringify({
                msg: "Succesfully created user account."
            }));
    }

    async createUser(username: string, password: string): Promise<string> {
        try {
            var con = await DbConnectionFactory.leaseConnection()
            let result = await con.execute("select * from users where username = ?", [username])
            if (result.rows.length > 0) {
                throw new UserResourceError(JSON.stringify({
                    msg: "Username already exists"
                }), 500)
            }
            var salt = await bcryptjs.genSalt(10)
            var hashedPassword = await bcryptjs.hash(password, salt)
            await con.execute("insert into users (username, password, salt) values (?, ?, ?)", [username, hashedPassword, salt])

            var token = await this.utils.createNewSessionToken(con)
            await this.utils.insertSingleToken(con, token, username)
            await DbConnectionFactory.returnConnection(con)

            return token
        }
        catch (err) {
            if (err instanceof UserResourceError) {
                throw err
            }
            throw new UserResourceError(JSON.stringify({
                msg: "Failed to create user account"
            }), 500)
        }
    }
}

export class ManageSession {
    req: Request;
    res: Response;
    utils: UserResourceUtilities = new UserResourceUtilities()

    constructor(req: Request, res: Response) {
        this.req = req
        this.res = res
    }

    async login() {
        var body = this.req.body
        var con = await DbConnectionFactory.leaseConnection()

        if (!(body.hasOwnProperty("username") && body.hasOwnProperty("password"))) {
            throw new UserResourceError(JSON.stringify({
                msg: "Missing username or password property."
            }), 422);
        }

        if (!(await UserValidation.verifyPassword(body["username"], body["password"], con))) {
            throw new UserResourceError(JSON.stringify({
                msg: "Failed to validate login information."
            }), 401)
        }

        var sessionToken: string = await this.utils.createNewSessionToken(con)
        await this.utils.insertSingleToken(con, sessionToken, body.username)
        this.res.status(200).setHeader("session-token", sessionToken).sendStatus(200)
        await DbConnectionFactory.returnConnection(con)
    }

    async logout() {
        var con = await DbConnectionFactory.leaseConnection()
        await this.utils.removeSingleToken(con, this.req.headers["session-token"] as string, this.res.locals.username)
        this.res.sendStatus(200)
        await DbConnectionFactory.returnConnection(con)
    }

    async logoutAll() {
        var con = await DbConnectionFactory.leaseConnection()
        await this.utils.deleteAllTokens(con, this.res.locals.username)
        this.res.sendStatus(200)
        await DbConnectionFactory.returnConnection(con)
    }

    static async deleteTokenViaUsername(username: string, con: cassandra.Client) {
        var currentTokenQuery = await con.execute("select accesstoken from users where username = ?", [username])
        if (currentTokenQuery.rows.length > 0) {
            var currentToken = currentTokenQuery.rows[0].accesstoken
            if (currentToken) {
                await con.execute("delete from sessions where accesstoken = ?", [currentToken])
                await con.execute("update users set accesstoken = ? where username = ?", ["", username])
            }
        }
    }
}

export class UserResourceUtilities {
    async insertSingleToken(con: Client, token: string, username: string) {
        var currentTokensResult = await con.execute("select accesstokens from users where username = ?", [username])
        var currentTokens = []
        if (currentTokensResult.rows.length > 0) {
            try {
                var currentTokensTemp = JSON.parse(currentTokensResult.rows[0].accesstokens)
                if (currentTokensTemp && currentTokensTemp.hasOwnProperty("length")) {
                    currentTokens = currentTokensTemp
                }
            }
            catch (e) {

            }
        }

        currentTokens.push(token)

        await con.execute("insert into sessions (accesstoken, username) values (?, ?)", [token, username])
        await con.execute("update users set accesstokens = ? where username = ?", [JSON.stringify(currentTokens), username])
    }

    async removeSingleToken(con: Client, token: string, username: string) {
        var currentTokensResult = await con.execute("select accesstokens from users where username = ?", [username])
        var currentTokens: Array<string> = []
        if (currentTokensResult.rows.length > 0) {
            try {
                var currentTokensTemp = JSON.parse(currentTokensResult.rows[0].accesstokens)
                if (currentTokensTemp && currentTokensTemp.hasOwnProperty("length")) {
                    currentTokens = currentTokensTemp
                }
            }
            catch (e) {

            }
        }

        currentTokens = currentTokens.filter((val: string, index: number) => {
            return val !== token
        })

        await con.execute("delete from sessions where accesstoken = ?", [token])
        await con.execute("update users set accesstokens = ? where username = ?", [JSON.stringify(currentTokens), username])
    }

    async createNewSessionToken(con: Client): Promise<string> {
        var token = ""
        var sessionTokenLength = 50
        do {
            token = crypto.randomBytes(Math.floor((sessionTokenLength * 6 / 8))).toString("base64")
            var result = await con.execute("select * from sessions where accesstoken = ?", [token])
            if (result.rows.length < 1) {
                break;
            }
        }
        while (true)

        return token;
    }

    async deleteAllTokens(con: Client, username: string) {
        var currentTokensResult = await con.execute("select accesstokens from users where username = ?", [username])
        var currentTokens: Array<string> = []
        if (currentTokensResult.rows.length > 0) {
            try {
                var currentTokensTemp = JSON.parse(currentTokensResult.rows[0].accesstokens)
                if (currentTokensTemp && currentTokensTemp.hasOwnProperty("length")) {
                    currentTokens = currentTokensTemp
                }
            }
            catch (e) {

            }
        }

        for (var token of currentTokens) {
            await con.execute("delete from sessions where accesstoken = ?", [token])
        }

        await con.execute("update users set accesstokens = '[]' where username = ?", [username])
    }

    async getUsernameFromToken(con: Client, token: string): Promise<string | null> {
        var usernameQuery = await con.execute("select username from sessions where accesstoken = ?", [token])
        if (usernameQuery.rows.length < 1) {
            return null;
        }
        else {
            return usernameQuery.rows[0].username
        }
    }
}

class UserResourceError extends Error {
    code: number = 500;
    msg: string = "";

    constructor(msg: string, code: number) {
        super(msg);
        this.msg = msg;
        this.code = code;
    }
}