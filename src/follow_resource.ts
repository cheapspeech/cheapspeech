import { Request, Response } from "express";
import DbConnectionFactory from "./utility/db_connection_factory"
import { Client } from "cassandra-driver"
import Logger from "./utility/logger";
import { UserResourceUtilities } from "./user_resource";

export default class FollowResource {

    static getFollowing(req: Request, res: Response, next: Function) {
        FollowResource.wrapCall(async () => {
            var obj = new GetFollowingInformation(req, res)
            await obj.getFollowing()
        }, res, next)
    }

    static getIsFollowing(req: Request, res: Response, next: Function) {
        FollowResource.wrapCall(async () => {
            var obj = new GetFollowingInformation(req, res)
            await obj.getIsFollowing()
        }, res, next)
    }

    static getFollowerCount(req: Request, res: Response, next: Function) {
        FollowResource.wrapCall(async () => {
            var obj = new GetFollowingInformation(req, res)
            await obj.getFollowerCount()
        }, res, next)
    }

    static follow(req: Request, res: Response, next: Function) {
        FollowResource.wrapCall(async () => {
            var obj = new FollowUsers(req, res)
            await obj.followUser()
        }, res, next)
    }

    static unfollow(req: Request, res: Response, next: Function) {
        FollowResource.wrapCall(async () => {
            var obj = new FollowUsers(req, res)
            await obj.unfollowUser()
        }, res, next)
    }

    static async wrapCall(callback: Function, res: Response, next: Function) {
        try {
            await callback()
        }
        catch (err) {
            FollowResource.errorResponse(res, err, next)
        }
    }

    static errorResponse(res: Response, err: any, next: Function) {
        if (err instanceof FollowResourceError) {
            res.status(err.code).type("application/json").send(err.msg)
        }
        else {
            Logger.appendErrorToLog(err)
            next(err)
        }
    }
}

class FollowUsers {
    req: Request;
    res: Response;

    constructor(req: Request, res: Response) {
        this.req = req
        this.res = res
    }

    async followUser() {
        this.validateCall()
        var con = await DbConnectionFactory.leaseConnection()
        var follower = this.res.locals.username
        var followed = this.req.body.username

        if (await this.isFollowingUser(con, follower, followed)) {
            throw new FollowResourceError(JSON.stringify({
                msg: "You are already following this user."
            }), 409)
        }


        var currentFollowerCountQuery = await con.execute("select followers from users where username = ?",
            [followed]
        )
        if (currentFollowerCountQuery.rows.length < 1) {
            throw new FollowResourceError(JSON.stringify({
                msg: "Could not find requested user."
            }), 404)
        }
        var followerCount = currentFollowerCountQuery.rows[0].followers ?? 0
        followerCount++

        await con.execute("update users set followers = ? where username = ?",
            [followerCount, followed],
            {
                hints: ["varint", "text"]
            }
        )
        await con.execute("insert into userfollowers (followerusername, followedusername) values (?, ?)",
            [follower, followed],
        )

        await DbConnectionFactory.returnConnection(con)
        this.res.sendStatus(200)
    }

    async unfollowUser() {
        this.validateCall()
        var con = await DbConnectionFactory.leaseConnection()
        var follower = this.res.locals.username
        var followed = this.req.body.username

        if (!(await this.isFollowingUser(con, follower, followed))) {
            throw new FollowResourceError(JSON.stringify({
                msg: "You are not following this user."
            }), 409)
        }

        var currentFollowerCountQuery = await con.execute("select followers from users where username = ?",
            [followed]
        )

        // If they have a record that says they are following this user we should allow them
        // to delete it even if that user no longer exists.  However if the user does not
        // exist we *cannot* update their record to increment the follower count.
        if (currentFollowerCountQuery.rows.length > 0) {
            var followerCount = currentFollowerCountQuery.rows[0].followers ?? 0
            followerCount--

            await con.execute("update users set followers = ? where username = ?",
                [followerCount, followed],
                {
                    hints: ["varint", "text"]
                }
            )
        }

        await con.execute("delete from userfollowers where followerusername = ? and followedusername = ?",
            [follower, followed],
        )

        await DbConnectionFactory.returnConnection(con)
        this.res.sendStatus(200)
    }

    validateCall() {
        if (!this.req.body.hasOwnProperty("username")) {
            throw new FollowResourceError(JSON.stringify({
                msg: "Missing `username` property from json body."
            }), 422)
        }
    }

    async isFollowingUser(con: Client, followerName: string, followedName: string): Promise<boolean> {
        var followingUserQuery = await con.execute("select * from userfollowers where followerusername = ? and followedusername = ?",
            [followerName, followedName]
        )

        return followingUserQuery.rows.length > 0
    }
}

class GetFollowingInformation {
    req: Request;
    res: Response;

    constructor(req: Request, res: Response) {
        this.req = req
        this.res = res
    }

    async getFollowerCount() {
        this.validateGetFollowerCount()
        var con = await DbConnectionFactory.leaseConnection()
        var username = await this.getUsernameForFollowerCount(con)
        var followerCountQuery = await con.execute("select followers from users where username = ?", [username])
        var followerCount: number
        if (followerCountQuery.rows.length > 1) {
            throw new FollowResourceError(JSON.stringify({
                msg: "Could not find user."
            }), 404)
        }
        else {
            followerCount = parseInt(followerCountQuery.rows[0].followers)
        }
        this.res.status(200).type("application/json").send(JSON.stringify({
            followerCount: followerCount
        }))
        await DbConnectionFactory.returnConnection(con)
    }

    async getUsernameForFollowerCount(con: Client): Promise<string> {
        if (this.req.query.hasOwnProperty("username") && this.req.query.username) {
            return this.req.query.username as string;
        }
        else {
            var sessionToken = this.req.headers["session-token"] as string
            var userUtilities = new UserResourceUtilities()
            var username = await userUtilities.getUsernameFromToken(con, sessionToken)
            if (!username) {
                throw new FollowResourceError(JSON.stringify({
                    msg: "Could not find user from token."
                }), 404)
            }

            return username;
        }
    }

    validateGetFollowerCount() {
        var hasUsername = this.req.query.hasOwnProperty("username")
        var hasSessionToken = this.req.headers.hasOwnProperty("session-token")
        if (!(hasUsername || hasSessionToken)) {
            throw new FollowResourceError(JSON.stringify({
                msg: "Must either include `username` query parameter or `session-token` header."
            }), 422)
        }
    }

    async getFollowing() {
        var con = await DbConnectionFactory.leaseConnection()
        var followingQuery = await con.execute("select followedusername from userfollowers where followerusername = ?",
            [this.res.locals.username]
        )
        var users: Array<string> = []
        for (var user of followingQuery.rows) {
            users.push(user.followedusername)
        }

        this.res.status(200).type("application/json").send(JSON.stringify({
            following: users
        }))

        await DbConnectionFactory.returnConnection(con)
    }

    async getIsFollowing() {
        this.validateGetIsFollowing()
        var con = await DbConnectionFactory.leaseConnection()
        var isFollowingQuery = await con.execute("select * from userfollowers where followerusername = ? and followedusername = ?",
            [
                this.res.locals.username,
                this.req.query.username
            ]
        )
        var isFollowing = isFollowingQuery.rows.length > 0
        this.res.status(200).type("application/json").send(JSON.stringify({
            isFollowing: isFollowing
        }))
        await DbConnectionFactory.returnConnection(con)
    }

    validateGetIsFollowing() {
        if (!this.req.query.hasOwnProperty("username")) {
            throw new FollowResourceError(JSON.stringify({
                msg: "Missing `username` query parameter."
            }), 422)
        }
    }
}

class FollowResourceError extends Error {
    code: number = 500;
    msg: string = "";

    constructor(msg: string, code: number) {
        super(msg);
        this.msg = msg;
        this.code = code;
    }
}